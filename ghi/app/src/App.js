import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Nav from './Nav';
import HatsList from './HatsList';
import HatsForm from './HatsForm';
import MainPage from './MainPage';
import ShoesList from "./ShoesList"; // Renamed to PascalCase
import ShoesForm from "./ShoesForm"; // Renamed to PascalCase

function App() {


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes" element={<ShoesList />} /> 
          <Route path="shoes/new" element={<ShoesForm />} /> 
          <Route path="/hats-form" element={<HatsForm />} />
          <Route path="/hats" element={<HatsList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

      {/* <Routes> */}
        {/* <Route path="/" element={<MainPage />} /> */}
      {/* </Routes> */}