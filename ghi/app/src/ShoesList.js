import React, { useState, useEffect } from 'react';

function ShoesList() {
    const [shoes, setShoes] = useState([]);

    useEffect(() => {
        fetchData();
    }, []);

    const fetchData = async () => {
        const shoesURL = "http://localhost:8080/api/shoes/";
        const shoesResponse = await fetch(shoesURL);
        if (shoesResponse.ok) {
            const data = await shoesResponse.json();
            console.log(data)
            setShoes(data.shoes);
        }
    };

    const deleteShoes = async (href) => {
        const fetchConfig = {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
            },
        };
        await fetch(`http://localhost:8080${href}`, fetchConfig);

        // Update the state to remove the deleted shoe
        window.location.reload()
    };

    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th>Shoe Name</th>
                        <th>Picture</th>
                        <th>Color</th>
                        <th>Bin</th>
                    </tr>
                </thead>
                <tbody>
                    {shoes.map(shoe => (
                        
                        <tr key={shoe.href}>
                            <td>{shoe.manufacturer}</td>
                            <td>{shoe.name}</td>
                            <td><img src={shoe.picture_url} style={{ width: "150", height: "auto"}}/></td>
                            <td>{shoe.color}</td>
                            <td>{shoe.bins}</td>
                            <td>
                                <button type="button" onClick={() => deleteShoes(shoe.href)} className="btn btn-danger">
                                    Delete
                                </button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default ShoesList;
