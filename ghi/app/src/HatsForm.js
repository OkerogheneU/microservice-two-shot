import React, { useState } from 'react';

const HatsForm = function () {
  const [newHat, setNewHat] = useState({
    fabric: '',
    styleName: '',
    color: '',
    pictureUrl: '',
    location: '',
  });

  const handleDelete = function () {

  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {

      const response = await fetch('http://localhost:8090/api/hats/', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newHat),
      });

      if (response.ok) {

        console.log('Hat created successfully');
      } else {

        console.error('Failed to create hat:', response.statusText);
      }
    } catch (error) {

      console.error('Error creating hat:', error);
    }
  };

  const handleInputChange = function (event) {
    const { name, value } = event.target;
    setNewHat({ ...newHat, [name]: value });
  };

  return (
    <div className="card" style={{ width: '25%', margin: '4px' }}>
      <img src={newHat.pictureUrl} className="card-img-top" alt={newHat.styleName} />
      <div className="card-body">
        <h6 className="card-title">
          {newHat.fabric} - {newHat.styleName}
        </h6>
        <div>Color: {newHat.color} </div>
        <div>Location: {newHat.location} </div>
        <button onClick={handleDelete} className="btn btn-lg btn-danger w-100">
          Delete Hat
        </button>
      </div>

      <form onSubmit={handleSubmit}>
        <h2>Create a New Hat</h2>
        <div>
          <label htmlFor="fabric">Fabric:</label>
          <input
            type="text"
            id="fabric"
            name="fabric"
            value={newHat.fabric}
            onChange={handleInputChange}
          />
        </div>
        <div>
          <label htmlFor="styleName">Style Name:</label>
          <input
            type="text"
            id="styleName"
            name="styleName"
            value={newHat.styleName}
            onChange={handleInputChange}
          />
        </div>
        <div>
          <label htmlFor="color">Color:</label>
          <input
            type="text"
            id="color"
            name="color"
            value={newHat.color}
            onChange={handleInputChange}
          />
        </div>
        <div>
          <label htmlFor="pictureUrl">Picture URL:</label>
          <input
            type="text"
            id="pictureUrl"
            name="pictureUrl"
            value={newHat.pictureUrl}
            onChange={handleInputChange}
          />
        </div>
        <button type="submit">Create Hat</button>
      </form>
    </div>
  );
};

export default HatsForm;
