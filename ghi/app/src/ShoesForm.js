import React, { useState, useEffect } from 'react';

function ShoesForm() {

    const [manufacturer, setManufacturer] = useState('');
    const [closet_name, setCloset_name] = useState('');
    const [picture_url, setPicture_url] = useState('');
    const [color, setColor] = useState('');
    const [selectedBin, setSelectedBin] = useState('')
    const [bins, setBins] = useState([]);


    const handleManufacturerChange = (event) => {
        setManufacturer(event.target.value);
    };

    const handleCloset_nameChange = (event) => {
        setCloset_name(event.target.value);
    };

    const handlePicture_urlChange = (event) => {
        setPicture_url(event.target.value);
    };

    const handleColorChange = (event) => {
        setColor(event.target.value);
    };

    const handleBin = (event) => {
        setSelectedBin(event.target.value);
    };


    // fetchData function to fetch the list of states
     // fetchData, useEffect, and return methods...
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data)
            setBins(data.bins); // ****
        }
    }

    // useEffect hook to call fetchData when the component mounts
    useEffect(() => {
        fetchData();
    }, []);



    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {
            manufacturer: manufacturer,
            name: closet_name,
            picture_url: picture_url,
            color: color,
            bin: selectedBin
        };

        console.log(data)

        const binUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(binUrl, fetchConfig);
        if (response.ok) {
            const newBin = await response.json();
            console.log(newBin);

            // Resetting the form fields
            setManufacturer('');
            setCloset_name('');
            setPicture_url('');
            setColor('');
            setSelectedBin('');
        }
    }


    // JSX for the LocationForm component
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new Shoe</h1>
                    <form onSubmit={handleSubmit} id="create-location-form">
                     <div className="form-floating mb-3">
                     <input 
                              onChange={handleManufacturerChange} 
                              placeholder="Manufacturer" 
                              required 
                              type="text" 
                              name="Manufacturer" 
                              id="Manufacturer" 
                              className="form-control" 
                              value={manufacturer}
                            />
                            <label htmlFor="name">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input 
                              onChange={handleCloset_nameChange} 
                              placeholder="Closet_namee" 
                              required 
                              type="text" 
                              name="Closet_name" 
                              id="Closet_name" 
                              className="form-control" 
                              value={closet_name}
                            />
                            <label htmlFor="room_count">Closet Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input 
                              onChange={handlePicture_urlChange} 
                              placeholder="Picture_url" 
                              required 
                              type="text" 
                              name="picture_url" 
                              id="picture_url" 
                              className="form-control" 
                              value={picture_url}
                            />
                            <label htmlFor="city">Picture</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input 
                              onChange={handleColorChange} 
                              placeholder="Color" 
                              required 
                              type="text" 
                              name="color" 
                              id="color" 
                              className="form-control" 
                              value={color}
                            />
                            <label htmlFor="city">Color</label>
                        </div>
                        <div className="mb-3">
                            <select 
                              onChange={handleBin} 
                              required 
                              name="bin" 
                              id="bin" 
                              className="form-select"
                              value={selectedBin} 
                            >
                                <option value="">Choose a bin</option>
                                {bins.map(bin => (
                                    <option key={bin.href} value={bin.id}>
                                        {bin.closet_name}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ShoesForm;
