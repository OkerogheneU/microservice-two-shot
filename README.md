# Wardrobify

Team:

* Person 1 - Which microservice? Hats - Gift Ugbeyide
* Person 2 - Which microservice? shoes - Yutong Ye

## Design

## Shoes microservice

#### Explain your models and integration with the wardrobe microservice, here.

-The Shoe model in the Wardrobify application includes attributes like manufacturer, model name, color, picture URL, and bin ID, providing a comprehensive framework for managing shoe data.

-This model integrates with the Wardrobe microservice, utilizing the Shoes Poller to synchronize shoe locations by retrieving bin data from the Wardrobe API.

-The Shoes API offers RESTful endpoints for listing, creating, and deleting shoes, allowing users to interactively manage their shoe collection in relation to their storage locations.



## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

The Hats Model required models that represents the Hat resource and the resource should keep track of the following properties.

Fabric: The material of the hat.
Style Name: The name or style designation of the hat.
Color: The color of the hat.
Picture URL: A URL pointing to an image of the hat.
Location: The location in the wardrobe where the hat exists.

The integration with the Wardrobe microservice involves implementing a poller in the hats/poll directory and the poller is responsible for pulling Location data from the Wardrobe API and updating the location information for hats.

I would start by creating the Django app, seeing that a Django app has already created.
