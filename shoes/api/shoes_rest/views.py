from django.shortcuts import render
from .models import Shoe, BinVO
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

# Create your views here.Do we always need "import_href"?
class BinVODetailEncoder(ModelEncoder):
    model=BinVO
    properties = ["closet_name", "import_href"]

#listEncoder almost always needs an ID
class ShoeListEncoder(ModelEncoder):
    model=Shoe
    properties = [
        "name",
        "picture_url",
        "id",
        "manufacturer",
        "color",
    ]

    # this is to include close_name from the related BinVO
    def get_extra_data(self, o):
        return {"bins": o.bin.closet_name} # refer to the closet name in the bin model 
    
class ShoeDetailEncoder(ModelEncoder):
    model=Shoe
    properties = [
        "manufacturer",
        "name",
        "color",
        "picture_url",
        "bin",
    ]
    #becuase bin info lives in another server 
    encoders = {
        "bin": BinVODetailEncoder(),
    }



@require_http_methods(["GET","POST"])
def api_list_shoes(request):  #request object that contains the raw request data sent by the client
   
    #GET all shoes 
    if request.method=="GET":
        shoes = Shoe.objects.all()
        #return response in Json format
        # get Json
        return JsonResponse(
            {"shoes": shoes},
            #This encoder specifies how instances of the Shoe model should be converted into a JSON-friendly format
            encoder=ShoeListEncoder,
        )
    
    #handle POST
    else: #handle raw HTTP request body client sends to the server
        content = json.loads(request.body) #json.loads will convert it into a Python dictionary


        try: #try allows you to catch specific exceptions 
            bin_href = f'/api/bins/{content["bin"]}/' # if  give if href ad value I can just use contend{bin}
            print(bin_href) #accessing key bin in the content
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"]=bin
            
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        
        shoe = Shoe.objects.create(**content)#allows you to dynamically pass different sets of parameters to the function without changing the function itself
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,#Using a custom encoder gives you control over how your data is represented in the JSON response
            safe=False,#it's okay return this non-dictionary object as JSON
        )

@require_http_methods(["GET","DELETE","PUT"])
def api_show_shoe(request, pk):
    if request.method=="GET":
        shoe = Shoe.objects.filter(id=pk) #pk is primary key
        return JsonResponse(
            {"shoe": shoe},
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    
    elif request.method=="DELETE":
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            return JsonResponse({"message": "Shoe has been deleted"})
        
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
        
    else: #PUT=EDIT
        try:
            content = json.loads(request.body)
            if "bin" in content: #check if bin key exist and prevent key error if not exist 
                #update the current bin content from the BinVO
                content["bin"] = BinVO.objects.get(id=content["bin"]) #accessing bin number in the key bin, which is an id 

            Shoe.objects.filter(id=pk).update(**content)
            shoe = Shoe.objects.get(id=pk) #selecting the one whose ID matches pk

            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
