from django.urls import path
from . import views 

urlpatterns = [
    #supports both GET and POST
    path('shoes/', views.api_list_shoes, name='api_list_shoes'),
    #supports GET, DELETE, and PUT
    path('shoes/<int:pk>/', views.api_show_shoe, name='api_show_shoe'),
]