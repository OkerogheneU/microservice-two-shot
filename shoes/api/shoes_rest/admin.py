from django.contrib import admin
from .models import Shoe

# Register your models here.

#register the Shoe model with Django's built-in admin site.
@admin.register(Shoe)
class ShoeAdmin(admin.ModelAdmin):
    pass