from django.db import models
from django.urls import reverse
# Create your models here.

class BinVO(models.Model):
    closet_name = models.CharField(max_length=100) #this needs to be the same as in the Bin model
    # Function to fetch data from Service B using import_href
    import_href = models.CharField(max_length=200,unique=True)

class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)
    #many-to-one relationship
    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE, #delete bin delete shoes
    )

    #create a custom string representation of a model instance
    def __str__(self):
        return self.name
    
    #to generate URLs for your model instances, reverse allow generate urls base on the view 
    #api_show_shoe is defined in the urls.py
    def get_api_url(self):
        return reverse("api_show_shoe", kwargs={"pk": self.pk})
