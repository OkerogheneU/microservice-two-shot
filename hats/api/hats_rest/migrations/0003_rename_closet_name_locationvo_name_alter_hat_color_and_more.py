# Generated by Django 4.0.3 on 2024-02-02 04:28

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0002_rename_location_hat_location'),
    ]

    operations = [
        migrations.RenameField(
            model_name='locationvo',
            old_name='closet_name',
            new_name='name',
        ),
        migrations.AlterField(
            model_name='hat',
            name='color',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='hat',
            name='location',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='hats', to='hats_rest.locationvo'),
        ),
        migrations.AlterField(
            model_name='locationvo',
            name='import_href',
            field=models.CharField(max_length=100, unique=True),
        ),
    ]
